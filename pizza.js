let quantite = Number(prompt('Combien de pizzas?'));
let choix = Number(prompt('1:fromage 15$, 2:poivron 16$, 3:végétarienne 14$'))
let sousTotal = 0;
let rabais = 0;
//calculer prix avant taxes et livraison
function calculerPrixBase(){
  switch(choix){
        case 1:
          return sousTotal = 15 * quantite;
        case 2: 
          return sousTotal = 16 * quantite;
        case 3:
         return sousTotal = 14 * quantite;
      }
}
//calculer rabais 20% sur montant avant livraison et taxes
function calculerRabais(){  
  if (sousTotal > 31) {
    rabais = parseFloat(sousTotal * 0.2).toFixed(2); 
    return rabais;
  }
  rabais = 0;
  return rabais;
}  
function afficherReduction(){
  let reduction = "";
  if(rabais != 0){  
  reduction = " (réduction de " + calculerRabais() + "$" + ")";
  return reduction;
  }
  return reduction;
}
// calcul prix livraison
let quartier = Number(prompt('Quel est votre quartier? 1: St-Jean, 2:Montcalm, 3: Limoilou'))
let fete = Number(prompt('Est-ce votre fête? 1: oui, 2: Non'));
let livraison = 0;
if (quartier === 1 || fete === 1){
  livraison = 0;
}
else{
  livraison = 5;
}
//calcul total avant taxes
let total = parseFloat(calculerPrixBase() - calculerRabais() + livraison).toFixed(2);
//calcul total avec taxes
let totalAvecTaxes = parseFloat(1.14975 * total).toFixed(2);
// affichage details commande
let commande = [];
commande [0] = "Nombre de pizzas: " + quantite + "<br>"
commande [1] = "Prix des pizzas: " + sousTotal + "$ " + "<br>"
commande [2] = "Prix livraison: " + livraison + "$" + "<br>"
commande [3] = "Prix total: " + total + "$" + afficherReduction() + "<br>"
commande [4] = "Prix total avec taxes: " + totalAvecTaxes + "$" + "<br>"
document.getElementById("pizza").innerHTML = commande.join("");


