function validation(){
//generation de chiffres au hasard entre 0 et 16
    function nombresAleatoires(min, max){ 
        return Math.floor(Math.random() * (max - min)) + min;
        }
        let chiffre1 = nombresAleatoires(0, 16); 
        let chiffre2 = nombresAleatoires(0, 16);
        let somme = chiffre1 + chiffre2;
//fenetre question nombres a caculer 
        let question = "";        
//tant que la reponse fournie par l'utilisateur est mauvaise, répéter la question (avec les mêmes chiffres) 
    while(question != somme){
        question = prompt("Quelle est la somme de " + chiffre1 + " + " + chiffre2 + "?");
//si l'utilisateur inscrit "q", sortir du programme 
        if(question === "q"){
            break;
         }  
//si la reponse fournie par l'utilisateur est bonne, generer une nouvelle question (avec des nouveaux chiffres)
         if(question == somme){
            validation();
        }  
    }
}
let hasard = validation();
